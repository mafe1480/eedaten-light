## EEDATEN LIGHT

Simple EEDATEN website with kepler.gl interactive maps. Not included are API layer and Postgresql/Postgis database model for requesting master data and maps in geojson format.

# Requirements and installation
Create folder with virtual environment and install django
```
mkdir base
cd base
virtualenv project
cd project
source bin/activate
pip install django
```
Clone repository to folder
and run server
```
python manage.py runserver
```