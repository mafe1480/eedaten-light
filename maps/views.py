from django.db.models.expressions import F
from django.shortcuts import render
from django.http import HttpResponse
from django.db.models import Sum
from django.core.serializers import serialize
from django.views.generic import TemplateView
import csv
from django.views.decorators.clickjacking import xframe_options_exempt


class Startseite(TemplateView):
    template_name = "karten/startseite.html"

class Datenschutzerklaerung(TemplateView):
    template_name = "karten/datenschutzerklaerung.html"

class Impressum(TemplateView):
    template_name = "karten/impressum.html"

class Sitemap(TemplateView):
    template_name = "karten/sitemap.xml"

@xframe_options_exempt
def solar_full_view(request):
    return render(request, 'karten/solar.html')

@xframe_options_exempt
def wind_full_view(request):
    return render(request, 'karten/wind.html')

@xframe_options_exempt
def biomasse_full_view(request):
    return render(request, 'karten/biomasse.html')

@xframe_options_exempt
def landkreis_full_view(request):
    return render(request, 'karten/ee_landkreis.html')
   
   
def solar_height(request):
    return render(request, 'karten/dashboard_solar.html')

def wind_height(request):
    return render(request, 'karten/dashboard_wind.html')

def biomasse_height(request):
    return render(request, 'karten/dashboard_biomasse.html')

def landkreis_height(request):
    return render(request, 'karten/dashboard_landkreis.html')