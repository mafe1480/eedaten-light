from django.urls import path
from . import views

urlpatterns = [
    path('', views.Startseite.as_view(), name="startseite"),
    path('solar_height', views.solar_height, name="solar_height"),
    path('wind_height', views.wind_height, name="wind_height"),
    path('biomasse_height', views.biomasse_height, name="biomasse_height"),
    path('landkreis_height', views.landkreis_height, name="landkreis_height"),
    path('solar_full', views.solar_full_view, name="solar_full_view"),
    path('wind_full', views.wind_full_view, name="wind_full_view"),
    path('biomasse_full', views.biomasse_full_view, name="biomasse_full_view"),
    path('landkreis_full', views.landkreis_full_view, name="landkreis_full_view"),
    path('datenschutzerklaerung', views.Datenschutzerklaerung.as_view(), name="datenschutzerklaerung"),
    path('impressum', views.Impressum.as_view(), name="impressum"),
]
